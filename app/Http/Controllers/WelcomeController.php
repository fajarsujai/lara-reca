<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        error_log('Welcome message here.');
        return view('welcome');
    }
}
