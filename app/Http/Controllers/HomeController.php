<?php

namespace App\Http\Controllers;

use App\Models\EngToIndo;
use App\Models\IndoToEng;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $engToIdn = EngToIndo::where('user_id',Auth::user()->id)->get();
        $idnToEng = IndoToEng::where('user_id',Auth::user()->id)->get();

        $countEngToIdn =  $engToIdn->count();
        $countIdnToEng = $idnToEng->count();
        return view('dashboard', compact('countEngToIdn', 'countIdnToEng'));
    }
}
