<?php

namespace App\Http\Controllers;

use App\Models\EngToIndo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class EngToIdnController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = EngToIndo::where('user_id',Auth::user()->id)->orderBy('word', 'asc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('pages.eng-idn.index');
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'word' => 'required|min:2',
            'kata' => 'required|min:2'            
        ]);
        if ($validator->fails()) {
            Log::info('Log error test');
            error_log('Some message here.');
            
            return response()->json(['message'=>$validator], 500);  
        }

        EngToIndo::updateOrCreate(['id' => $request->Item_id],
        [
            'user_id' => Auth::user()->id,
            'word' => $request->word,
            'kata' => $request->kata 
        ]);        

        return response()->json(['message'=>'Data created.'], 201);
    }

    public function edit($id)
    {
        $item = EngToIndo::find($id);
        return response()->json($item);
    }

    public function destroy($id)
    {
        EngToIndo::find($id)->delete();
       return response()->json(['success'=>'Data deleted successfully.']);
    }
}
