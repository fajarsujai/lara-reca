<?php

namespace App\Http\Controllers;

use App\Models\IndoToEng;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class IdnToEngController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = IndoToEng::where('user_id',Auth::user()->id)->latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('pages.idn-eng.index');
    }

    public function store(Request $request)
    {
        IndoToEng::updateOrCreate(['id' => $request->Item_id],
        [
            'user_id' => Auth::user()->id,
            'word' => $request->word,
            'kata' => $request->kata    
        ]);        

        return response()->json(['success'=>'Data saved successfully.']);
    }

    public function edit($id)
    {
        $item = IndoToEng::find($id);
        return response()->json($item);
    }

    public function destroy($id)
    {
        IndoToEng::find($id)->delete();
       
        return response()->json(['success'=>'Data deleted successfully.']);
    }
}
