<?php

use App\Http\Controllers\EngToIdnController;
use App\Http\Controllers\IdnToEngController;
use App\Http\Controllers\SynonimEngController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	// eng to idn
	// Route::resource('eng-idn','EngToIdnController');
	Route::get('eng-idn', [EngToIdnController::class, 'index']);
	Route::post('eng-idn/store', [EngToIdnController::class, 'store']);
	Route::get('eng-idn/{id}/edit', [EngToIdnController::class, 'edit']);
	Route::delete('eng-idn/delete/{id}', [EngToIdnController::class, 'destroy']);

	//
	// Route::resource('syn-eng-idn','SynonimEngController');
	Route::get('syn-eng-idn', [SynonimEngController::class, 'index']);
	Route::post('syn-eng-idn/store', [SynonimEngController::class, 'store']);
	Route::get('syn-eng-idn/{id}/edit', [SynonimEngController::class, 'edit']);
	Route::delete('syn-eng-idn/delete/{id}', [SynonimEngController::class, 'destroy']);

	// idn to eng
	// Route::resource('idn-eng','IdnToEngController');
	Route::get('idn-eng', [IdnToEngController::class, 'index']);
	Route::post('idn-eng/store', [IdnToEngController::class, 'store']);
	Route::get('idn-eng/{id}/edit', [IdnToEngController::class, 'edit']);
	Route::delete('idn-eng/delete/{id}', [IdnToEngController::class, 'destroy']);

});

