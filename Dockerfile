FROM php:7.4-fpm

RUN apt-get update -y && apt-get install -y apt-utils libpq-dev libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev libonig-dev \
    libfreetype6-dev libzip-dev zlib1g-dev \
    build-essential \
    locales \
    jpegoptim optipng pngquant gifsicle \
    ca-certificates \
    vim \
    tmux \
    unzip \
    git \
    cron \
    supervisor \
    curl


# RUN ext pgsql & mysql
RUN docker-php-ext-install mbstring zip gd pdo pdo_pgsql mysqli pdo_mysql
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
# RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
# RUN docker-php-ext-install pdo pdo_pgsql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


WORKDIR /app

COPY . /app
RUN cp .env.example .env
RUN composer install

EXPOSE 8000
ENTRYPOINT ["sh","run.sh"]
